#include <bits/stdc++.h>
using namespace std;


struct{
	const long long MOD = 1e9+7;
	vector<long long> solutions;

	void answer(long long x){
		solutions.push_back(x%MOD);
	}

	void print_solution() {
		long long M = 1000000007;
		long long res = 0;
		long long p = 13;
		for (long long sol : solutions){
			res = (res + (p*sol)%M)%M;
			p = (p*13L)%M;
			while (res<0) res+=M;
		}
		cout << res << '\n';
	}

} solutionEngine;

string label;
int q;
long long a,b;
int main() {
	cin >> label;
	cin >> q;
	for (int i=0;i<q;++i) {
		cin >> a >> b;
		solutionEngine.answer(a-b);
	}

	solutionEngine.print_solution();
	return 0;
}