import java.util.List;
import java.util.ArrayList;

public class AMinusBSolutionHelper {

    static class SolutionEngine{
        private static List<Long> solutions = new ArrayList<>();
        private static final long MOD = 1000000007;
        static void answer(long x) {
            solutions.add(x%MOD);
        }

        static void print_solution() {
            long M = 1000000007;
            long res = 0;
            long p = 13;
            for (long sol : solutions) {
                res = (res + (p*sol)%M)%M;
                p = (p*13L)%M;
            }
            while (res<0) res+=M;
            System.out.println(res);
        }
    }


    // Untuk memberikan jawaban, cukup panggil method answer(x) dimana x adalah jawaban dari suatu pertanyaan.
    // Pastikan method print_solution() dipanggil di bagian paling akhir dari main.
    // Anda cukup menyalin hasil keluaran dari method print_solution() kedalam template solusi yang diberikan.
    
    public static void main(String[] args) {
        // Lakukan proses input dan komputasi disini

        SolutionEngine.print_solution();
    }
}