#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string LABEL;
	int Q;
	vector<int> A,B;
	int ANS;
	
	void InputFormat() {
		LINE(LABEL);
		LINE(Q);
		LINES(A, B) % SIZE(Q);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= Q && Q <= 1000);
		CONS(eachElementBetween(A, -100000, 100000));
		CONS(eachElementBetween(B, -100000, 100000));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"4",
				"20 18",
				"-56 0",
				"0 0",
				"-54 -20"});
		Output({"999019495"});
	}

	void BeforeTestCase() {
		A.clear();
		B.clear();
	}

	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			CASE(create_label(i), Q = rnd.nextInt(800, 1000), randomAB(Q));
		}
	}

private:

	void randomAB(int Q) {
		while (A.size() < Q) {
			A.push_back(rnd.nextInt(-100000, 100000));
			B.push_back(rnd.nextInt(-100000, 100000));
		}
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
};