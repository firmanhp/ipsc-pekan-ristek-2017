#include <bits/stdc++.h>
using namespace std;

// Masukkan jawaban anda untuk setiap kasus uji didalam variabel dibawah ini,
// Misalkan jawaban anda untuk kasus uji nomor 2 adalah 3, maka baris dibawah akan menjadi 'long long jawaban_2 = 3;' (tanpa tanda petik)
// Apabila jawaban Anda melebihi batasan tipe data int pada C++, sisipkan karakter LL setelah angka jawaban anda.
// Contoh: 'long long jawaban = 3000000000LL; ' (tanpa tanda petik)
long long jawaban_0 = 13;
long long jawaban_1 = ;
long long jawaban_2 = ;
long long jawaban_3 = ;
long long jawaban_4 = ;
long long jawaban_5 = ;

int main() {
	string label;
	cin >> label;
	if (label == "#0") cout << jawaban_0 << '\n';
	if (label == "#1") cout << jawaban_1 << '\n';
	if (label == "#2") cout << jawaban_2 << '\n';
	if (label == "#3") cout << jawaban_3 << '\n';
	if (label == "#4") cout << jawaban_4 << '\n';
	if (label == "#5") cout << jawaban_5 << '\n';

	return 0;
}