#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	string X;
	int ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(X);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= X.size() && X.size() <= 100000);
		CONS(noLeadingZero(X));
		CONS(charsBetween(X, '0', '9'));
	}

private:
	bool noLeadingZero(string &S) {
		return S[0] != '0';
	}

	bool charsBetween(string &S, char lo, char hi) {
		for (char c : S) {
			if (c < lo || c > hi)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
			"9"});
		Output({"89"});
	}

	void BeforeTestCase() {
		X = "";
	}

	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			CASE(create_label(i), constructRandomNumber(rnd.nextInt(90000, 100000)));
		}
	}

private:

	void constructRandomNumber(int digits) {
		bool firstDigit = true;
		for (int i=0;i<digits;i++) {
			char x;
			do {
				x = rnd.nextInt(0,9)+'0';
			} while (firstDigit && x=='0');
			X.push_back(x);
		}
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}

};