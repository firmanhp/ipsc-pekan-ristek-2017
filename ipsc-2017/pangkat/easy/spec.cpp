#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int X;
	int ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(X);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= X && X <= 15);
	}

private:

};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
			"9"});
		Output({"89"});
	}

	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			CASE(create_label(i), X = 5 + (2*i));
		}
	}

private:

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};