#include <bits/stdc++.h>
using namespace std;

#define FAST_CC ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);

//nilai default jika elemen ga ada, be = begin, en = end, l = lampu, g = gedung
const int bel = -2;
const int beg = -1;
const int enl = 1000000002;
const int eng = 1000000001;

map<int, set<int> > grow, gcol, lrow, lcol;
vector<pair<int, int> > maling;
int x, y;
string label;

int main() {
	FAST_CC
	int n, m, k, b;
	cin >> label;
	cin >> n >> m >> k >> b;

	//Input
	for (int i = 0; i < m; i++) {
		cin >> x >> y;
		if (lrow[x].find(y) == lrow[x].end()) {
			//dummy begin untuk lampu
			lrow[x].insert(-2);
		}
		lrow[x].insert(y);

		if(lcol[y].find(x) == lcol[y].end()) {
			//dummy begin untuk lampu
			lcol[y].insert(-2);
		}
		lcol[y].insert(x);
	}

	for (int i = 0; i < k; i++) {
		cin >> x >> y;
		maling.push_back(make_pair(x, y));
	}

	for (int i = 0; i < b; i++) {
		cin >> x >> y;
		if (grow[x].find(y) == grow[x].end()) {
			//dummy begin untuk gedung
			grow[x].insert(-1);
		}
		grow[x].insert(y);

		if (gcol[y].find(x) == gcol[y].end()) {
			//dummy begin untuk gedung
			gcol[y].insert(-1);
		}
		gcol[y].insert(x);
	}

	//Solve
	int ans = 0;
	set<int>::iterator it;
	for (int i = 0; i < (int)maling.size(); i++) {
		x = maling[i].first;
		y = maling[i].second;

		it = lrow[x].upper_bound(y);
		if (it != lrow[x].begin()) it--;
		int lrowleft = (it == lrow[x].end()? bel : *it);

		it = lrow[x].upper_bound(y);
		int lrowright = (it == lrow[x].end()? enl : *it);

		it = lcol[y].upper_bound(x);
		if (it != lcol[y].begin()) it--;
		int lcoltop = (it == lcol[y].end()? bel : *it);

		it = lcol[y].upper_bound(x);
		int lcoldown = (it == lcol[y].end()? enl : *it);

		it = grow[x].upper_bound(y);
		if (it != grow[x].begin()) it--;
		int growleft = (it == grow[x].end()? beg : *it);

		it = grow[x].upper_bound(y);
		int growright = (it == grow[x].end()? eng : *it);

		it = gcol[y].upper_bound(x);
		if (it != gcol[y].begin()) it--;
		int gcoltop = (it == gcol[y].end()? beg : *it);

		it = gcol[y].upper_bound(x);
		int gcoldown = (it == gcol[y].end()? eng : *it);

		if (lrowleft > growleft || lrowright < growright || lcoltop > gcoltop || lcoldown < gcoldown) {
			ans++;
		}
	}
	printf("%d\n", ans);
	return 0;
}
