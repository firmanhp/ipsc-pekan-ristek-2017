#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> pii;
enum p {
	BLACK,
	PERSON,
	LAMP,
};
typedef enum p p_t;

set<pii> shined;

struct point{
	int x,y;
	p_t state;
};

inline bool cmp_leftright(point a, point b) {
	return (a.y == b.y) ? (a.x < b.x) : (a.y < b.y);
}

inline bool cmp_rightleft(point a, point b) {
	return (a.y == b.y) ? (a.x < b.x) : (a.y > b.y);
}

inline bool cmp_updown(point a, point b) {
	return (a.x == b.x) ? (a.y < b.y) : (a.x < b.x);
}

inline bool cmp_downup(point a, point b) {
	return (a.x == b.x) ? (a.y < b.y) : (a.x > b.x);
}

vector<point> points;
int n,m,k,b;
int b_x,b_y,p_x,p_y,l_x,l_y;
string label;

void line_sweep_x(vector <point> &pnts) { // utk linesweep atas bawah
	map<int,bool> lights_on;
	for (point p : pnts) {
		if (p.state == LAMP) lights_on[p.y] = 1;
		if (p.state == BLACK) lights_on[p.y] = 0;
		if (p.state == PERSON && lights_on[p.y]) shined.insert({p.x,p.y});
	}
}

void line_sweep_y(vector <point> &pnts) { // utk linesweep kiri kanan
	map<int,bool> lights_on;
	for (point p : pnts) {
		if (p.state == LAMP) lights_on[p.x] = 1;
		if (p.state == BLACK) lights_on[p.x] = 0;
		if (p.state == PERSON && lights_on[p.x]) shined.insert({p.x,p.y});
	}
}


int main() {
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> label;
	cin >> n >> m >> k >> b;
	for (int i=0;i<m;++i) {
		cin >> l_x >> l_y;
		points.push_back((point){l_x, l_y, LAMP});
	}
	for (int i=0;i<k;++i) {
		cin >> p_x >> p_y;
		points.push_back((point){p_x, p_y, PERSON});
	}
	for (int i=0;i<b;++i) {
		cin >> b_x >> b_y;
		points.push_back((point){b_x, b_y, BLACK});
	}

	sort(points.begin(), points.end(), cmp_leftright);
	line_sweep_y(points);

	sort(points.begin(), points.end(), cmp_rightleft);
	line_sweep_y(points);

	sort(points.begin(), points.end(), cmp_updown);
	line_sweep_x(points);

	sort(points.begin(), points.end(), cmp_downup);
	line_sweep_x(points);
	
	cout << shined.size() << '\n';

	return 0;
}