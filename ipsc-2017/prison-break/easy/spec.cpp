#include <tcframe/spec.hpp>
using namespace tcframe;
typedef pair<int,int> pii;

class ProblemSpec : public BaseProblemSpec {
protected:
	string LABEL;
	int N,M,K,B;
	vector<int> LX, LY, PX, PY, BX, BY;
	int ANS;

	void InputFormat() {
		LINE(LABEL);
		LINE(N, M, K, B);
		LINES(LX, LY) % SIZE(M);
		LINES(PX, PY) % SIZE(K);
		LINES(BX, BY) % SIZE(B);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(3);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(3 <= N && N <= 1000);
		CONS(1 <= M && M <= 1000);
		CONS(1 <= K && K <= 1000);
		CONS(1 <= B && B <= 1000);
		CONS(3 <= (M+K+B) && (M+K+B) <= (N*N));
		CONS(eachElementBetween(LX, 1, N));
		CONS(eachElementBetween(LY, 1, N));
		CONS(eachElementBetween(PX, 1, N));
		CONS(eachElementBetween(PY, 1, N));
		CONS(eachElementBetween(BX, 1, N));
		CONS(eachElementBetween(BY, 1, N));
		CONS(sameSize(LX, LY));
		CONS(sameSize(PX, PY));
		CONS(sameSize(BX, BY));
		CONS(noSharedPoints(LX, LY, PX, PY, BX, BY));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}

	bool sameSize(vector<int> &a, vector<int> &b) {
		return (a.size() == b.size());
	}

	bool noSharedPoints(vector<int> &x1, vector<int> &y1, vector<int> &x2, vector<int> &y2, vector<int> &x3, vector<int> &y3) {
		set<pii> points;
		for (int i=0;i<(int)x1.size();++i) {
			if (points.find({x1[i],y1[i]}) != points.end())
				return false;
			points.insert({x1[i],y1[i]});
		}


		for (int i=0;i<(int)x2.size();++i) {
			if (points.find({x2[i],y2[i]}) != points.end())
				return false;
			points.insert({x2[i],y2[i]});
		}


		for (int i=0;i<(int)x3.size();++i) {
			if (points.find({x3[i],y3[i]}) != points.end())
				return false;
			points.insert({x3[i],y3[i]});
		}

		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"6 2 3 2",
				"3 2",
				"1 4",
				"1 2",
				"3 6",
				"1 6",
				"5 4",
				"3 4",});
		Output({"2"});
	}

	void AfterTestCase() {
		vector<int> rndVector, LXrnd(M), LYrnd(M), PXrnd(K), PYrnd(K), BXrnd(B), BYrnd(B);
		for (int i=0;i<M;++i) rndVector.push_back(i);
		rnd.shuffle(rndVector.begin(), rndVector.end());
		for (int i=0;i<M;++i) {
			LXrnd[i] = LX[rndVector[i]];
			LYrnd[i] = LY[rndVector[i]];
		}
		rndVector.clear();	
		for (int i=0;i<K;++i) rndVector.push_back(i);
		rnd.shuffle(rndVector.begin(), rndVector.end());
		for (int i=0;i<K;++i) {
			PXrnd[i] = PX[rndVector[i]];
			PYrnd[i] = PY[rndVector[i]];
		}

		rndVector.clear();	
		for (int i=0;i<B;++i) rndVector.push_back(i);
		rnd.shuffle(rndVector.begin(), rndVector.end());
		for (int i=0;i<B;++i) {
			BXrnd[i] = BX[rndVector[i]];
			BYrnd[i] = BY[rndVector[i]];
		}
		swap(LX, LXrnd); swap(LY, LYrnd);
		swap(PX, PXrnd); swap(PY, PYrnd);
		swap(BX, BXrnd); swap(BY, BYrnd);
	}

	void BeforeTestCase() {
		LX.clear(); LY.clear();
		PX.clear(); PY.clear();
		BX.clear(); BY.clear();
		usedPoints.clear();
	}



	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			int n = rnd.nextInt(900, 1000);
			int nSquared = n*n;
			int m = rnd.nextInt(500,min(1000, nSquared));
			int k = rnd.nextInt(500,min(1000, nSquared-m));
			int b = rnd.nextInt(500,min(1000, nSquared-m-k));
			CASE(create_label(i), N = n, M = m, K = k, B = b, generate(N, M, K, B));
		}
	}

private:
	set<pii> usedPoints;

	enum pattern { // L = lamp, P = person, B = black box
		FREE = 0, LPB = 1, LBP = 2, PLB = 3, PBL = 4, BLP = 5, BPL = 6, LPL = 7,
	};
	typedef enum pattern pattern;

	void generate(int n, int m, int k, int b) {
		while (LX.size() < m && PX.size() < k && BX.size() < b) {
			pattern pattern_type = (pattern)rnd.nextInt(0,7);
			int b_x,b_y,p_x,p_y,l_x,l_y;
			int tries=0;
			bool is_vertical = rnd.nextInt(0,1);

			if (is_vertical) l_y = p_y = b_y = rnd.nextInt(1, n); // set fixed value for one axis
			else l_x = p_x = b_x = rnd.nextInt(1, n);

			if (pattern_type == FREE) { putRandom(n); continue; }
			else if (pattern_type == LPB) {
				if (!findPoint(n, l_x, l_y, is_vertical, 1, 1, 2)) { putRandom(n); continue; }
				if (!findPoint(n, p_x, p_y, is_vertical, l_x+1, l_y+1, 1)) { putRandom(n); continue; }
				if (!findPoint(n, b_x, b_y, is_vertical, p_x+1, p_y+1, 0)) { putRandom(n); continue; }
			} else if (pattern_type == LBP) {
				if (!findPoint(n, l_x, l_y, is_vertical, 1, 1, 2)) { putRandom(n); continue; }
				if (!findPoint(n, b_x, b_y, is_vertical, l_x+1, l_y+1, 1)) { putRandom(n); continue; }
				if (!findPoint(n, p_x, p_y, is_vertical, b_x+1, b_y+1, 0)) { putRandom(n); continue; }
			} else if (pattern_type == PLB) {
				if (!findPoint(n, p_x, p_y, is_vertical, 1, 1, 2)) { putRandom(n); continue; }
				if (!findPoint(n, l_x, l_y, is_vertical, p_x+1, p_y+1, 1)) { putRandom(n); continue; }
				if (!findPoint(n, b_x, b_y, is_vertical, l_x+1, l_y+1, 0)) { putRandom(n); continue; }
			} else if (pattern_type == PBL) {
				if (!findPoint(n, p_x, p_y, is_vertical, 1, 1, 2)) { putRandom(n); continue; }
				if (!findPoint(n, b_x, b_y, is_vertical, p_x+1, p_y+1, 1)) { putRandom(n); continue; }
				if (!findPoint(n, l_x, l_y, is_vertical, b_x+1, b_y+1, 0)) { putRandom(n); continue; }
			} else if (pattern_type == BLP) {
				if (!findPoint(n, b_x, b_y, is_vertical, 1, 1, 2)) { putRandom(n); continue; }
				if (!findPoint(n, l_x, l_y, is_vertical, b_x+1, b_y+1, 1)) { putRandom(n); continue; }
				if (!findPoint(n, p_x, p_y, is_vertical, l_x+1, l_y+1, 0)) { putRandom(n); continue; }
			} else if (pattern_type == BPL) {
				if (!findPoint(n, b_x, b_y, is_vertical, 1, 1, 2)) { putRandom(n); continue; }
				if (!findPoint(n, p_x, p_y, is_vertical, b_x+1, b_y+1, 1)) { putRandom(n); continue; }
				if (!findPoint(n, l_x, l_y, is_vertical, p_x+1, p_y+1, 0)) { putRandom(n); continue; }
			} else if (pattern_type == LPL) {
				int l2_x = l_x ,l2_y = l_y;
				if (m-LX.size() < 2) { continue; }
				if (!findPoint(n, l_x, l_y, is_vertical, 1, 1, 2)) { putRandomLPL(n); continue; }
				if (!findPoint(n, p_x, p_y, is_vertical, l_x+1, l_y+1, 1)) { putRandomLPL(n); continue; }
				if (!findPoint(n, l2_x, l2_y, is_vertical, p_x+1, p_y+1, 0)) { putRandomLPL(n); continue; }
				usedPoints.insert({l_x,l_y});
				usedPoints.insert({p_x,p_y});
				usedPoints.insert({l2_x,l2_y});
				LX.push_back(l_x); LY.push_back(l_y);
				PX.push_back(p_x); PY.push_back(p_y);
				LX.push_back(l2_x); LY.push_back(l2_y);
				continue;
			}

			usedPoints.insert({l_x,l_y});
			usedPoints.insert({p_x,p_y});
			usedPoints.insert({b_x,b_y});
			LX.push_back(l_x); LY.push_back(l_y);
			PX.push_back(p_x); PY.push_back(p_y);
			BX.push_back(b_x); BY.push_back(b_y);
			continue;
		}

		while (LX.size() < m) putRandomL(n);
		while (PX.size() < k) putRandomP(n);
		while (BX.size() < b) putRandomB(n);
			
	}

	bool findPoint(int n, int &x, int &y, bool is_vertical, int starting_x, int starting_y, int margin) {
		int tries = 0;
		do {
			++tries; if (tries > 5) return false;
			if (is_vertical) x = rnd.nextInt(starting_x, n-margin);
			else y = rnd.nextInt(starting_y, n-margin);
		} while (usedPoints.find({x, y}) != usedPoints.end());
		return true;
	}

	void putRandomL(int n) {
		int x,y;
		do {
			x = rnd.nextInt(1, n);
			y = rnd.nextInt(1, n);
		} while (usedPoints.find({x,y}) != usedPoints.end());
		usedPoints.insert({x,y});
		LX.push_back(x);
		LY.push_back(y);
	}

	void putRandomP(int n) {
		int x,y;
		do {
			x = rnd.nextInt(1, n);
			y = rnd.nextInt(1, n);
		} while (usedPoints.find({x,y}) != usedPoints.end());
		usedPoints.insert({x,y});
		PX.push_back(x);
		PY.push_back(y);
	}

	void putRandomB(int n) {
		int x,y;
		do {
			x = rnd.nextInt(1, n);
			y = rnd.nextInt(1, n);
		} while (usedPoints.find({x,y}) != usedPoints.end());
		usedPoints.insert({x,y});
		BX.push_back(x);
		BY.push_back(y);
	}

	void putRandom(int n) {
		putRandomL(n);
		putRandomP(n);
		putRandomB(n);
	}

	void putRandomLPL(int n) {
		putRandomL(n);
		putRandomP(n);
		putRandomL(n);
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

};