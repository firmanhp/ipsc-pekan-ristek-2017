#include <bits/stdc++.h>
#define fi first
#define se second
using namespace std;
typedef pair<int,int> pii;

bool is_blackbox[1005][1005];
bool is_person[1005][1005];

string label;
int n,m,k,b;
int b_x,b_y,p_x,p_y,l_x,l_y;
set<pii> shined;
vector<pii> lamps;

int main() {
	cin >> label;
	cin >> n >> m >> k >> b;
	for (int i=0;i<m;++i) {
		cin >> l_x >> l_y;
		lamps.push_back({l_x, l_y});
	}
	for (int i=0;i<k;++i) {
		cin >> p_x >> p_y;
		is_person[p_x][p_y]=true;
	}
	for (int i=0;i<b;++i) {
		cin >> b_x >> b_y;
		is_blackbox[b_x][b_y]=true;
	}
	
	for (pii xy : lamps) {
		l_x = xy.fi;
		while (l_x>0 && !is_blackbox[l_x][xy.se]) {
			if (is_person[l_x][xy.se]) shined.insert({l_x,xy.se});
			--l_x;
		}
		l_x = xy.fi;
		while (l_x<=n && !is_blackbox[l_x][xy.se]) {
			if (is_person[l_x][xy.se]) shined.insert({l_x,xy.se});
			++l_x;
		}
		l_y = xy.se;
		while (l_y>0 && !is_blackbox[xy.fi][l_y]) {
			if (is_person[xy.fi][l_y]) shined.insert({xy.fi,l_y});
			--l_y;
		}
		l_y = xy.se;
		while (l_y<=n && !is_blackbox[xy.fi][l_y]) {
			if (is_person[xy.fi][l_y]) shined.insert({xy.fi,l_y});
			++l_y;
		}
	}

	cout << shined.size() << '\n';
	return 0;
}