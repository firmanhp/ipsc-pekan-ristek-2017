import requests
from bs4 import BeautifulSoup
import time
import json

label = input()
n = int(input())
result = []
while n > 0 :
	n -= 1
	query = input()
	url = "http://judgels-competition.cs.ui.ac.id/ieos/find"

	print("getting sequence with url {}".format(url))
	resp = requests.post(url, data={'q' : query})

	if(resp.status_code != 200):
		print("err on {}".format(query))

	soup = BeautifulSoup(resp.text, 'html.parser')
	idx = soup.select(".media-left strong")[0].text
	result.append(int(idx[1:]))

	# sleep(2) # biar gak terlalu cepet

result.sort()
print("A{} A{} A{}".format(str(result[0]).zfill(6), str(result[len(result)-1]).zfill(6), str(result[len(result)//2]).zfill(6)))
