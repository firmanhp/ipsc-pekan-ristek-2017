import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

class TCGenPair{
    int first, second;

    public TCGenPair(int first, int second) {
        this.first = first;
        this.second = second;
    }
}

public class TCGenerator {
    static final int CONFIG_N = 20; // atur disini
    static final int CONFIG_M = 100; // atur disini

    static int N, M;
    static List<Integer> U = new ArrayList<>(), V = new ArrayList<>();

    static Random rnd = new Random();

    static int rndInt(int from, int to) { // menghasilkan bilangan acak dalam interval [from,to]
        assert from<=to;
        return from + rnd.nextInt(to-from+1);
    }

    static void constructGraph(int n, int m, int X, int Y) {
        Set<Integer> XS = new HashSet<>(), YS = new HashSet<>();
        List<Integer> XA = new ArrayList<>(), YA = new ArrayList<>();

        while (XS.size() < X) {
            int candid;
            do {
                candid = rndInt(1, n);
            } while (XS.contains(candid));
            XS.add(candid);
            XA.add(candid);
        }

        while (YS.size() < Y) {
            int candid;
            do {
                candid = rndInt(1, n);
            } while (XS.contains(candid) || YS.contains(candid));
            YS.add(candid);
            YA.add(candid);
        }

        for (int i=1;i<=n;++i) {
            if (!XS.contains(i) && !YS.contains(i)) {
                if (rndInt(0, 1000) > 500)
                    XA.add(i);
                else
                    YA.add(i);
            }
        }

        N = n;
        M = m;
        Set<TCGenPair> edgeSet = new HashSet<>();
        while (U.size() < M && V.size() < M) {
            int u,v;
            do {
                u = XA.get(rndInt(0, XA.size() - 1));
                v = YA.get(rndInt(0, YA.size() - 1));
            } while (edgeSet.contains(new TCGenPair(u,v)));
            edgeSet.add(new TCGenPair(u,v));
            edgeSet.add(new TCGenPair(v,u));
            U.add(u);
            V.add(v);
        }
    }

    static void config(int N, int maxM) {
        int bcM = 0, bcX = -1, bcY = -1;
        for (int i=0; (bcX == -1 || bcY == -1 || i<128); ++i) {
            int X = rndInt(0, N);
            int Y = rndInt(0, N-X);
            if (X*Y <= maxM && X+Y <= N && X*Y >= 2) {
                bcX = X;
                bcY = Y;
                bcM = rndInt((X*Y)/2, X*Y);
            }
        }
        constructGraph(N, bcM, bcX, bcY);
    }

    public static void main(String[] args) {
        config(CONFIG_N, CONFIG_M);
        System.out.println(N + " " + M);
        for (int i=0;i<M;++i) {
            System.out.println(U.get(i) + " " + V.get(i));
        }
    }

}
