#include <bits/stdc++.h>
using namespace std;

const int N = 15;

int n, m, visit[N+5], ID[N+5];
vector<int> adj[N+5], kiri, kanan;

void dfs(int u, int step) {
	if (step % 2 == 0) {
		kiri.push_back(u);
	} else {
		kanan.push_back(u);
	}

	visit[u] = 1;
	for (int i = 0; i < adj[u].size(); i++) {
		int v = adj[u][i];
		if (!visit[v])
			dfs(v, step+1);
	}
}

int memo[N+5][(1 << N) + 5];
int f(int n, int mask) {
	if (n == kiri.size())
		return 0;

	int &ret = memo[n][mask];
	if (ret != -1) return ret;

	ret = f(n+1, mask);
	int u = kiri[n];

	for (int i = 0; i < adj[u].size(); i++) {
		int v = adj[u][i];
		if (((1 << ID[v]) & mask) == 0) {
			ret = max(ret, 1 + f(n+1, (mask | (1 << ID[v]))));
		}
	}
	return ret;
}

int main() {
	char dummy[10];
	scanf("%s", dummy);
	scanf("%d %d", &n, &m);
	for (int i = 0; i < m; i++) {
		int u, v;
		scanf("%d %d", &u, &v);
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	for (int i = 1; i <= n; i++) {
		if (!visit[i])
			dfs(i, 0);
	}

	for (int i = 0; i < kanan.size(); i++) {
		ID[kanan[i]] = i;
	}

	memset(memo, -1, sizeof(memo));
	int ans = f(0, 0);
	printf("%d\n", ans);
}