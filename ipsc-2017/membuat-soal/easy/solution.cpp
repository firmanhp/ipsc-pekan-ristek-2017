#include <bits/stdc++.h>
#define fi first
#define se second
using namespace std;
typedef pair<int,int> pii;

int n,m;
int u,v;
vector<pii> edges;
int dp[110][1<<18];
string label;

inline bool is_on(int mask, int idx) {
	return (mask&(1<<idx)) != 0;
}

int calc(int now, int mask) {
	if (now == edges.size())
		return 0;
	int &ret = dp[now][mask];
	if (ret!=-1)
		return ret;
	ret = calc(now+1,mask);
	pii &edge = edges[now];
	if (!is_on(mask, edge.fi) && !is_on(mask, edge.se))
		ret = max(ret, 1+calc(now+1, mask|(1<<edge.fi)|(1<<edge.se)));

	return ret;
}

int main() {
	memset(dp,-1,sizeof dp);

	cin >> label;
	cin >> n >> m;
	for (int i=0;i<m;++i) {
		cin >> u >> v;
		--u; --v;
		edges.push_back({u,v});
	}
	cout << calc(0,0) << '\n';
	return 0;
}