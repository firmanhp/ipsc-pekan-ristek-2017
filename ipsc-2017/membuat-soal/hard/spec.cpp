#include <tcframe/spec.hpp>
using namespace tcframe;
typedef pair<int,int> pii;

class ProblemSpec : public BaseProblemSpec {
protected:
	string LABEL;
	int N, M;
	vector<int> U, V;
	int ANS;

	void InputFormat() {
		LINE(LABEL);
		LINE(N, M);
		LINES(U, V) % SIZE(M);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(3 <= N && N <= 2000);
		CONS(1 <= M && M <= min(10000, (N*(N-1))/2));
		CONS(eachElementBetween(U, 1, N));
		CONS(eachElementBetween(V, 1, N));
		CONS(bipartiteGraph(N,U,V));
		CONS(noDuplicateEdges(U,V));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}

	int colors[2005]; // 1 and 0, bipartite
	vector<int> adjlist[2005];
	bool dfs(int u, int color) {
		colors[u] = color;
		for (int v : adjlist[u]) {
			if (colors[v] == color)
				return false;
			if (colors[v] == -1)
				if (!dfs(v, 1-color))
					return false;

		}
		return true;
	}

	bool bipartiteGraph(int N, vector<int> &U, vector<int> &V) {
		memset(colors, -1, sizeof colors);
		for (int i=0;i<2005;++i)
			adjlist[i].clear();

		for (int i=0;i<U.size();++i) {
			adjlist[U[i]].push_back(V[i]);
			adjlist[V[i]].push_back(U[i]);
		}
		for (int i=1;i<=N;i++)
			if (colors[i] == -1)
				if (!dfs(i,0))
					return false;
		return true;
	}

	bool noDuplicateEdges(vector<int> &U, vector<int> &V) {
		set<pii> edges;
		for (int i=0;i<U.size();++i) {
			if (edges.find({U[i], V[i]}) != edges.end())
				return false;
			edges.insert({U[i], V[i]});

			if (edges.find({V[i], U[i]}) != edges.end())
				return false;
			edges.insert({V[i], U[i]});
		}

		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"6 7",
				"4 6",
				"3 4",
				"4 2",
				"5 6",
				"1 3",
				"2 1",
				"5 3",});
		Output({"3"});
	}

	void BeforeTestCase() {
		U.clear();
		V.clear();
	}

	void TestGroup1() {
		CASE(create_label(1), N = 3, generateBipartiteConfig(N, 2));
		for (int i=2;i<=9;++i) {
			CASE(create_label(i), N = rnd.nextInt(100,2000), generateBipartiteConfig(N,10000));
		}
		CASE(create_label(10), N = 2000, generateBipartiteConfig(N, 10000));
	}

private:

	void constructGraph(int n, int m, int X, int Y) {
		set<int> XS,YS;
		vector<int> XA,YA;
		while (XS.size() < X) {
			int candid;
			do {
				candid = rnd.nextInt(1,n);
			} while (XS.find(candid) != XS.end());
			XS.insert(candid);
			XA.push_back(candid);
		}

		while (YS.size() < Y) {
			int candid;
			do {
				candid = rnd.nextInt(1,n);
			} while (XS.find(candid) != XS.end() || YS.find(candid) != YS.end());
			YS.insert(candid);
			YA.push_back(candid);
		}

		for (int i=1;i<=n;++i) {
			if (XS.find(i) == XS.end() && YS.find(i) == YS.end()) {
				if (rnd.nextInt(0,1000) < 500)
					XA.push_back(i);
				else
					YA.push_back(i);
			}
		}

		N = n;
		M = m;
		set<pii> edgeSet;
		while (U.size() < M && V.size() < M) {
			int u,v;
			do {
				u = XA[rnd.nextInt(0, XA.size() - 1)];
				v = YA[rnd.nextInt(0, YA.size() - 1)];
			} while (edgeSet.find({u,v}) != edgeSet.end());
			edgeSet.insert({u,v});
			edgeSet.insert({v,u});
			U.push_back(u);
			V.push_back(v);
		}
	}

	void generateBipartiteConfig(int N, int maxM) {
		int bestChoiceM = 0, bestChoiceX = -1, bestChoiceY = -1;
		for (int i=0;(bestChoiceX == -1 || bestChoiceY == -1 || i<128);++i) {
			int X = rnd.nextInt(0, N);
			int Y = rnd.nextInt(0, N-X);
			if (X*Y <= maxM && X+Y <= N && X*Y >= 2) {
				bestChoiceX = X;
				bestChoiceY = Y;
				bestChoiceM = rnd.nextInt((X*Y)/2, X*Y);
			}
		}
		constructGraph(N, bestChoiceM, bestChoiceX, bestChoiceY);
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

};