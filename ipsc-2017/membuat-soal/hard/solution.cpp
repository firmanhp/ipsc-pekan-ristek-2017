#include <bits/stdc++.h>
#define SOURCE 0
#define SINK 2004
using namespace std;

vector<int> adjlist[2005];
int last[2005], cap[2005][2005];
bool bfs(int source, int sink) {
	queue<int> bfq;
	memset(last, -1, sizeof last);
	bfq.push(source);
	while (!bfq.empty()) {
		int u = bfq.front(); bfq.pop();
		if (u == sink)
			return true;
		for (int v : adjlist[u]) {
			if (last[v] != -1 || (cap[u][v] == 0))
				continue;
			last[v] = u;
			bfq.push(v);
		}

	}
	return false;
}


int n,m;
int _u,_v;
int colors[2005];
int ans;

void dfs(int u, int color) { // 0 left, 1 right (bipartite)
	colors[u] = color;
	for (int v : adjlist[u]) {
		assert(colors[v] != color);
		if (colors[v] == -1)
			dfs(v, 1-color); // flip
	}
}

string label;
int main() {
	memset(colors, -1, sizeof colors); // not set
	cin >> label;
	cin >> n >> m;
	for (int i=0;i<m;++i) {
		cin >> _u >> _v;
		adjlist[_u].push_back(_v);
		adjlist[_v].push_back(_u);
	}
	
	for (int i=1;i<=n;++i)
		if (colors[i] == -1)
			dfs(i, 0);

	for (int u=1;u<=n;++u) {
		if (colors[u] == 0) {
			for (int v : adjlist[u]) {
				cap[u][v] = 1;
			}
			adjlist[SOURCE].push_back(u);
			adjlist[u].push_back(SOURCE);
			cap[SOURCE][u] = 1;
		} else {
			adjlist[SINK].push_back(u);
			adjlist[u].push_back(SINK);
			cap[u][SINK] = 1;
		}
	}

	ans=0;
	while (bfs(SOURCE, SINK)) {
		int now = SINK;
		++ans;
		while (now != SOURCE) {
			cap[last[now]][now] = 0;
			cap[now][last[now]] = 1;
			now = last[now];
		}
	}

	cout << ans << '\n';
	return 0;
}