#include <bits/stdc++.h>
using namespace std;

const int N = 2000;

char dummy[10];
int n, m, owner[N+5], visit[N+5];
vector<int> adj[N+5], kiri;

void dfs(int u, int step) {
	if (step % 2 == 0) {
		kiri.push_back(u);
	}

	visit[u] = 1;
	for (int i = 0; i < adj[u].size(); i++) {
		int v = adj[u][i];
		if (!visit[v])
			dfs(v, step+1);
	}
}

int altPath(int u) {
	if (visit[u]) return 0;
	visit[u] = 1;

	for (int i = 0; i < adj[u].size(); i++) {
		int v = adj[u][i];
		if (owner[v] == -1 || altPath(owner[v])) {
			owner[v] = u;
			return 1;
		}
	}

	return 0;
}

int main() {
	scanf("%s", dummy);
	scanf("%d %d", &n, &m);
	for (int i = 0; i < m; i++) {
		int u, v;
		scanf("%d %d", &u, &v);
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	for (int i = 1; i <= n; i++) {
		if (!visit[i]) {
			dfs(i, 0);
		}
	}

	int ans = 0;
	memset(owner, -1, sizeof(owner));

	for (int i = 0; i < kiri.size(); i++) {
		int u = kiri[i];
		memset(visit, 0, sizeof(visit));

		ans += altPath(u);
	}

	printf("%d\n", ans);
}