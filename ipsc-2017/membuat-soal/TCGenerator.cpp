#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <utility>
#include <set>
#include <vector>
#define mp make_pair
using namespace std;
typedef pair<int, int> pii;

vector<int> U,V;
int N, M;

int rnd(int from, int to) { // menghasilkan bilangan acak dalam interval [from,to]
	return from + (rand() % (to-from+1));
}

void constructGraph(int n, int m, int X, int Y) {
	set<int> XS,YS;
	vector<int> XA,YA;
	while (XS.size() < X) {
		int candid;
		do {
			candid = rnd(1,n);
		} while (XS.find(candid) != XS.end());
		XS.insert(candid);
		XA.push_back(candid);
	}

	while (YS.size() < Y) {
		int candid;
		do {
			candid = rnd(1,n);
		} while (XS.find(candid) != XS.end() || YS.find(candid) != YS.end());
		YS.insert(candid);
		YA.push_back(candid);
	}

	for (int i=1;i<=n;++i) {
		if (XS.find(i) == XS.end() && YS.find(i) == YS.end()) {
			if (rnd(0,1000) < 500)
				XA.push_back(i);
			else
				YA.push_back(i);
		}
	}

	N = n;
	M = m;
	set<pii> edgeSet;
	while (U.size() < M && V.size() < M) {
		int u,v;
		do {
			u = XA[rnd(0, XA.size() - 1)];
			v = YA[rnd(0, YA.size() - 1)];
		} while (edgeSet.find(mp(u,v)) != edgeSet.end());
		edgeSet.insert(mp(u,v));
		edgeSet.insert(mp(v,u));
		U.push_back(u);
		V.push_back(v);
	}
}

void config(int N, int maxM) {
	int bcM = 0, bcX = -1, bcY = -1;
	for (int i=0;(bcX == -1 || bcY == -1 || i<128);++i) {
		int X = rnd(0, N);
		int Y = rnd(0, N-X);
		if (X*Y <= maxM && X+Y <= N && X*Y >= 2) {
			bcX = X;
			bcY = Y;
			bcM = rnd((X*Y)/2, X*Y);
		}
	}
	constructGraph(N, bcM, bcX, bcY);
}

const int CONFIG_N = 20; // atur disini
const int CONFIG_M = 100; // atur disini

int main() {
	srand(time(NULL));
	config(CONFIG_N, CONFIG_M);
	cout << N << ' ' << M << '\n';
	for (int i=0;i<M;++i) {
		cout << U[i] << ' ' << V[i] << '\n';
	}
	return 0;
}