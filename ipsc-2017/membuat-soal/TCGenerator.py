from random import seed, randint
seed()

U = list()
V = list()
N = 0
M = 0
def constructGraph(n, m, X, Y):
	global N, M, U, V
	XS = set()
	YS = set()
	XA = list()
	YA = list()
	while len(XS) < X:
		candid = randint(1, n)
		while candid in XS:
			candid = randint(1, n)
		XS.add(candid)
		XA.append(candid)

	while len(YS) < Y:
		candid = randint(1, n)
		while candid in XS or candid in YS:
			candid = randint(1, n)
		YS.add(candid)
		YA.append(candid)

	for i in range(1, n+1):
		if (i not in XS and i not in YS):
			if randint(0, 1000) < 500:
				XA.append(i)
			else:
				YA.append(i)

	N = n
	M = m
	edgeSet = set()
	while len(U) < M and len(V) < M:
		u = XA[randint(0, len(XA) - 1)]
		v = YA[randint(0, len(YA) - 1)]
		while (u,v) in edgeSet:
			u = XA[randint(0, len(XA) - 1)]
			v = YA[randint(0, len(YA) - 1)]
		edgeSet.add((u,v))
		edgeSet.add((v,u))
		U.append(u)
		V.append(v)

def config(N, maxM):
	bcM = 0
	bcX = -1
	bcY = -1
	i = 0
	while bcX == -1 or bcY == -1 or i < 128:
		X = randint(0, N)
		Y = randint(0, N-X)
		if X*Y <= maxM and X+Y <= N and X*Y >= 2:
			bcX = X
			bcY = Y
			bcM = randint(X*Y//2, X*Y)
		i+=1

	constructGraph(N, bcM, bcX, bcY)


CONFIG_N = 20 # atur disini
CONFIG_M = 100 # atur disini

config(CONFIG_N, CONFIG_M)
print(N, M)
for i in range(M):
	print(U[i], V[i])