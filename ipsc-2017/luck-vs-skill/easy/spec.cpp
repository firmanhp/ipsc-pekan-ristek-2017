#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int N;
	
	void InputFormat() {
	}

	void OutputFormat() {
		LINE(N);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
		CustomScorer();
	}

	void Constraints() {
		CONS(8 <= N && N <= 8);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	
	void TestGroup1() {
		CASE(N = 8);
	}

private:
};