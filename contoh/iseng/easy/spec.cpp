#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	long long N;
	string S;
	
	void InputFormat() {
		LINE(N);
	}

	void OutputFormat() {
		LINE(S);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
		CustomScorer();
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000000000);
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"42"});
		Output({"Pedro"});
	}

	void TestGroup1() {
		CASE(N = 1);
		CASE(N = 2);
		CASE(N = 3);
		CASE(N = 4);
		CASE(N = 6);
		CASE(N = 9);
		CASE(N = 24);
		CASE(N = 55);
		CASE(N = 91);
		CASE(N = 127);
		CASE(N = 484);
		CASE(N = 777);
		CASE(N = 3121);
		CASE(N = 4242);
		CASE(N = 7396);
		CASE(N = 47524);
		CASE(N = 541589);
		CASE(N = 7124099);
		CASE(N = 81029196);
		CASE(N = 751198464);
	}

private:
};